import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Student } from './student.entity';
import { CreateStudentInput } from './student.input';
import { v4 as uuid } from 'uuid';

@Injectable()
export class StudentService {
  constructor(
    @InjectRepository(Student) private studentRepository: Repository<Student>,
  ) {}

  async createStudent(
    createStudentInput: CreateStudentInput,
  ): Promise<Student> {
    const { name } = createStudentInput;
    const student = this.studentRepository.create({
      id: uuid(),
      name,
    });

    return await this.studentRepository.save(student);
  }

  async getStudent(id: string): Promise<Student> {
    return await this.studentRepository.findOne({ id });
  }

  async getAllStudents(): Promise<Array<Student>> {
    return await this.studentRepository.find();
  }

  async getManyStudents(studentIds: string[]): Promise<Student[]> {
    return await this.studentRepository.find({
      where: {
        id: {
          $in: studentIds,
        },
      },
    });
  }
}
